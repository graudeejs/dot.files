. "$HOME/.generic_sh"

if [ "${OS:-`uname`}" == "Darwin" ]; then
   export PATH=/usr/local/mysql/bin:$PATH
   export DYLD_LIBRARY_PATH=/usr/local/mysql/lib/
fi

export PATH="$HOME/.rbenv/bin:$PATH"

# vim: set ts=4 sw=4 ft=sh:

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
