begin
  require 'irbtools'
rescue
  print("failed to require irbtools")
end


def try_require(lib, &callback)
  if require lib
    yield if block_given?
  end
rescue
  return false
end

def reload
  reload!
end


# vim: set ft=ruby:
