#!/bin/sh

# This script must be run from dot.files directory.
# It will DELETE some dot files from HOME directory and replace them with links
# to dot.files

# This file ententionally is not set +x, to run it, in shell type:
#	sh link_files.sh

DOT_DIR="`pwd`"

OS="${OS:-`uname`}"
case "$OS" in
'FreeBSD' | 'Dariwn' )
	DOT_FILES="`find . -name "dot.*" -depth 1 | sed 's#^\./##'`"
	DOT_CONFIG_FILES="`find config -depth 1`"
	BIN_FILES="`find bin -depth 1`"
	;;
* )
	DOT_FILES="`find . -name "dot.*" -maxdepth 1 | sed 's#^\./##'`"
	DOT_CONFIG_FILES="`find config -maxdepth 1`"
	BIN_FILES="`find bin -maxdepth 1`"
	;;
esac


## copy file to directory
# "dot" prefix will be removed
# Arguments:
#   $1 - src file/directory/symbolic link
#   $2 - dest directory
copy_file() {
  if [ "$1" = '' -o "$2" = '' ]; then
    echo "Argument error" > /dev/stderr
    exit 1
  fi


  FILE_NAME="`echo "$1" | sed 's#.*/##; s/^dot\././'`"

  echo "$2/$FILE_NAME"
  rm -Rf "$2/$FILE_NAME"
  if [ -L "$DOT_DIR/$1" ]; then
    cp -RP "$DOT_DIR/$1" "$2/$FILE_NAME"
  else
    ln -s "$DOT_DIR/$1" "$2/$FILE_NAME"
  fi
}

# handle dotfiles in ~/
for FILE in $DOT_FILES; do
  copy_file "$FILE" "$HOME"
done

# handle dotfiles in ~/.config
[ -d "$HOME/.config" ] || mkdir -p "$HOME/.config"
for FILE in $DOT_CONFIG_FILES; do
  copy_file "$FILE" "$HOME/.config"
done

# handle ~/bin files
[ -d "$HOME/bin" ] || mkdir -p "$HOME/bin"
for FILE in $BIN_FILES; do
  copy_file "$FILE" "$HOME/bin"
done

# This speeds app startup times
# http://kdemonkey.blogspot.com/2008/04/magic-trick.html
[ -d "$HOME/.compose-cache" ] || mkdir -p "$HOME/.compose-cache"
