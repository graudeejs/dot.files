if [ "$WHOAMI" != "`whoami`" ]; then
    . "$HOME/.common_env"
fi

export GPG_TTY="$(tty)"
if which vim > /dev/null; then
    export EDITOR=vim
fi


if [ "$WINDOWID" != '' -a "$DISPLAY" != '' ]; then
    alias ag="ag --color-path '1;34' --color-line-number '1;31'"
fi
alias bc='bc -l'
alias hexdump="hexdump -C"
alias less="less -i -m -X"
alias du="du -h"
alias df="df -h"
alias sdate="date '+%Y-%m-%d'"
alias ssdate="date '+%Y%m%d'"
alias timestamp="date '+%s'"
alias mounto="mount -o ro"
alias smount="sudo mount"
alias sumount="sudo umount"
alias diff="diff -u"
alias when='when --monday_first --noampm'
alias mathomatic='mathomatic -b'
alias scilab='scilab -nwni -nb'
alias rsync="rsync --progress"
alias su="su -l"
alias fspec="rspec --only-failures"

alias tag="date '+%Y-%m-%d'"

alias diffnames="git diff --name-only"
alias branch_info="git branch --edit-description"
alias stats="git diff master --stat"
alias gadd="git add -v"
alias gc="git commit -v"
alias co="git checkout"
alias rebase="git pull --rebase origin master"

alias tarsnap="tarsnap --keyfile ${HOME}/.tarsnap/keys/secrets.key --cachedir ${HOME}/.tarsnap/cache"

case "$OS" in

'FreeBSD' )
    [ "$WHOAMI" != 'root' ] && gpg-connect-agent /bye
    alias ls='ls -Gh'
    alias gelia="sudo geli attach"
    alias gelid="sudo geli detach"
    alias mutt="neomutt"
    mail -e && echo "You got mail"
    ;;

'Darwin' )
    alias ls='ls -Gh'
    ;;

'Linux' )
    [ "$WHOAMI" != 'root' ] && gpg-connect-agent /bye
    alias ls='ls -h --color'
    alias gpg='gpg2'
    alias mutt="neomutt"
    ;;

esac


case "$TERM" in
*"-256color")
    alias tmux="tmux -f ${HOME}/.tmux-256color.conf"
    ;;
esac


if [ "$WHOAMI" = 'root' ]; then
    alias vim="vim -X"
    export PS1="root@$(hostname) # "
else
    export PS1="$WHOAMI@$(hostname) $ "
fi

realhome() {
	awk 'BEGIN { FS=":" }; /^'"$WHOAMI"':/ {print $6}' /etc/passwd
}

mkdircd() {
    mkdir -p "$1"
    cd "$1"
}

mkdatedir() {
    mkdir "`date "+%Y.%m.%d"`"
}

# vim:set ft=sh ts=4 sw=4:
