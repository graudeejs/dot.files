[ -f /etc/csh.cshrc ] && source /etc/csh.cshrc

set path = (/sbin /bin /usr/sbin /usr/bin /usr/games /usr/local/sbin /usr/local/bin /usr/local/kde4/bin $HOME/bin)

setenv LANG "en_GB.UTF-8"

setenv BLOCKSIZE   K

#which vim > /dev/null && setenv EDITOR vim || setenv EDITOR vi
# setenv VIMRUNTIME /usr/local/share/vim/vim74

setenv XAUTHORITY $HOME/.Xauthority
setenv XDG_CONFIG_HOME $HOME/.config
setenv LSCOLORS "cxexfxgxbxdxdxDbDxDada"
which vim > /dev/null && setenv EDITOR vim || setenv EDITOR vi
setenv PAGER 'less -i -m'

# file permissions: rwxr-xr-x
#
# umask	022

#alias sudo	sudo -E
alias bc	bc -l
alias hexdump	hexdump -C
alias less	less -i -m -X
alias ls	ls -FG
alias sdate	date '+%Y.%m.%d'
alias ssdate	date '+%Y%m%d'
alias stime	date '+%s'
alias rv	vim --remote-silent

which mathomatic > /dev/null && alias mathomatic	mathomatic -b
which scilab > /dev/null && 	alias scilab		scilab -nwni
which when > /dev/null &&	alias when		when --monday_first --noampm

# Read system messages
msgs -f
# Allow terminal messages
#mesg y

if ($?prompt) then
    # An interactive shell -- set some stuff up
    if ( "`whoami`" == 'root' ) then
        set prompt = "root@%m % "
        alias vim vim -X
    else
        set prompt = "%m % "
    endif
    set filec
    set history = 100
    set savehist = 100
    set mail = (/var/mail/$USER)
    if ( $?tcsh ) then
        bindkey "^W" backward-delete-word
        bindkey -k up history-search-backward
        bindkey -k down history-search-forward
    endif
endif

# vim: set ft=csh:
