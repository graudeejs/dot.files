begin
  require 'pry-rails' unless defined? Pry
  Pry.commands.alias_command 'c', 'continue'
  Pry.commands.alias_command 's', 'step'
  Pry.commands.alias_command 'n', 'next'
  Pry.commands.alias_command 'quit!', 'quit-program'
rescue => e
  puts e.inspect
end

# vim: set ft=ruby:
