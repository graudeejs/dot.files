export OS="`uname`"
export WHOAMI="`whoami`"
export XAUTHORITY="$HOME/.Xauthority"
export XDG_CONFIG_HOME="$HOME/.config"

export LC_ALL="en_GB.UTF-8"
export LC_COLLATE='C' # https://forums.freebsd.org/threads/59876/
export EDITOR=vi
export MANWIDTH=80
export FTP_PASSIVE_MODE=yes

#                1 2 3 4 5 6 7 8 9 A B
export LSCOLORS="cxexfxgxbxdxdxDbDxDada"
# http://geoff.greer.fm/lscolors/
# http://www.bigsoft.co.uk/blog/index.php/2008/04/11/configuring-ls_colors
export LS_COLORS="di=32:ln=34:so=35:pi=36:ex=31:bd=33:cd=33:su=1;33;41:sg=1;33:tw=1;33:ow=33:"

export PAGER="less -i -m -X"
export VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

extend_path() {
    if [ -d "$1" ]; then
        if [ "$PATH" = '' ]; then
            echo "$1"
        else
            echo "$PATH:$1"
        fi
    else
        echo "$PATH"
    fi
}

prepend_path() {
    if [ -d "$1" ]; then
        if [ "$PATH" = '' ]; then
            echo "$1"
        else
            echo "$1:$PATH"
        fi
    else
        echo "$PATH"
    fi
}

if [ ! -d /etc/nixos ]; then
    PATH=""
fi

if [ -d "$HOME/src/gocode" ]; then
    export GOPATH="$HOME/src/gocode"
    PATH=$(prepend_path "$GOPATH/bin")
fi
PATH=$(prepend_path "$HOME/.local/bin")

if [ ! -d /etc/nixos ]; then
    PATH=$(prepend_path "$HOME/bin") # NixOS is configured to add this
    PATH=$(extend_path "/usr/local/sbin")
    PATH=$(extend_path "/usr/local/bin")
    PATH=$(extend_path "/usr/local/kde4/bin")
    PATH=$(extend_path "/usr/sbin")
    PATH=$(extend_path "/usr/bin")
    PATH=$(extend_path "/usr/games")
    PATH=$(extend_path "/usr/pkg/sbin") # minix
    PATH=$(extend_path "/usr/pkg/bin")  # minix
    PATH=$(extend_path "/usr/bin/core_perl")
    PATH=$(extend_path "/opt/bin")
    PATH=$(extend_path "/sbin")
    PATH=$(extend_path "/bin")
fi

export PATH


case "$OS" in
'FreeBSD' )
    export CHROME_BIN=/usr/local/bin/chrome

    # gnuplot needs this
    export GDFONTPATH=/usr/local/lib/X11/fonts
    export GNUPLOT_DEFAULT_GDFONT=Liberation/LiberationSans-Regular.ttf
    ;;

* )
    if which lsb_release 2>&1 > /dev/null && lsb_release -i | grep Gentoo 2>&1 > /dev/null; then
        # Gentoo doesn't like arguments for pager
        export PAGER=less
    fi
    ;;

esac


# PATH="${PATH}:$HOME/.local/bin"

# PATH="$HOME/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;

if [ "$SSH_CONNECTION" = '' ]; then
    export SSH_AUTH_SOCK="$(gpgconf --list-dir agent-ssh-socket)"
fi

export DISABLE_DATABASE_ENVIRONMENT_CHECK=1 # https://github.com/jalkoby/squasher/issues/27#issuecomment-281205120


# set ENV to a file invoked each time sh is started for interactive use.
ENV=$HOME/.shrc; export ENV

# http://docs.ansible.com/ansible/latest/faq.html#how-do-i-disable-cowsay
export ANSIBLE_NOCOWS=1

# # https://github.com/vagrant-libvirt/vagrant-libvirt#start-vm
# export VAGRANT_DEFAULT_PROVIDER=libvirt

# http://docs.ansible.com/ansible/latest/faq.html#how-do-i-disable-cowsay
export ANSIBLE_NOCOWS=1
export ANSIBLE_VAULT_PASSWORD_FILE="$HOME/bin/ansible_vault_password.sh"

# vim: set ft=sh:
