#!/bin/sh
for FILE in "$@"; do
    gpg --decrypt "$FILE" | tar -xf -
done
