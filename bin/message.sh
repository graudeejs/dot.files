#!/bin/sh

if which gxmessage > /dev/null; then
  exec gxmessage -font "dejavu sans mono" "$@"
else
  exec xmessage "$@"
fi
