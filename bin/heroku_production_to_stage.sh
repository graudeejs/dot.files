#!/bin/sh

if [ "$1" = '' ]; then
    echo "Usage:" > /dev/stderr
    echo "$0 stage2" > /dev/stderr
    exit 1
fi

heroku pg:backups:restore "`heroku pg:backups:public-url --remote production`" DATABASE_URL --remote "$1"

heroku run rake db:migrate --remote "$1"
heroku restart --remote "$1"
