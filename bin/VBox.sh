#!/bin/sh

# "THE BEER-WARE LICENSE":
# Aldis Berjoza wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.  <graudeejs@gmail.com>


# Script to manage VirtualBox on FreeBSD (Needs more work)
# Arg1 - start|stop|status

[ "${OS:-`uname`}" = 'FreeBSD' ] || exit 1

case "$1" in
'start')
	kldstat | grep -q vboxdrv || {
		sudo kldload vboxdrv
#		sudo /usr/local/etc/rc.d/vboxnet onestart
	}
	;;

'stop')
	kldstat | grep -q vboxdrv && {
#		sudo /usr/local/etc/rc.d/vboxnet onestop
		sudo kldunload vboxdrv
	}
	;;

#'restart')
#	kldstat | grep -q vboxdrv && sudo /usr/local/etc/rc.d/vboxnet onerestart
#	;;

'status')
	kldstat | grep -q vboxdrv && echo 'vboxdrv loaded' || echo 'vboxdrv not loaded'
	;;

*)
	echo "`basename $0` [ start | stop | status | restart ]"
	;;

esac
