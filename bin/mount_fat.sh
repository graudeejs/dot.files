#!/bin/sh

if [ $# -ne 2 ]; then
    echo "`basename $0` usage:" > /dev/stderr
    echo "`basename $0` device mount_point" > /dev/stderr
    exit 1
fi

case "${OS:-`uname`}" in

'FreeBSD' )
    if which -s kiconvtool; then
        sudo kldload msdosfs_iconv
        sudo kiconvtool -f CP1257 -l UTF-8
        mount_msdosfs -L lv_LV.UTF-8 -D CP1257 "$1" "$2"
    else
        echo "kiconvtool is missing" > /dev/stderr
        echo "install sysutils/kiconvtool" > /dev/stderr
        exit 1
    fi
    ;;

*)
    echo "Not implemented" > /dev/stderr
    exit 1
    ;;

esac
