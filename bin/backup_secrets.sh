#!/bin/sh

PC="secrets"

cd "$HOME"

/usr/local/bin/tarsnap \
    -c -f "${PC}-$(date +%Y-%m-%d_%H-%M-%S)" \
    --keyfile "${HOME}/.tarsnap/keys/${PC}.key" \
    --cachedir "${HOME}/.tarsnap/cache" \
    ".gnupg" \
    ".rclone.conf" \
    ".ssh" \
    ".password-store" \
    "doc/router" \
    "doc/ssl"
