#!/bin/sh

mkdir smaller
for IMAGE in $*; do
    convert $IMAGE -resize 1200x1200\> smaller/$IMAGE
done

cd smaller
watermark.sh $*
