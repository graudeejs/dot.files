#!/bin/sh

# Generic menu
# No args

HOSTNAME="`hostname`"
MENU_APP=${MENU_APP:-dmenu}
TERMINAL=terminator

[ "$WMSCRIPTS_EXEC_PWD" ] && cd "$WMSCRIPTS_EXEC_PWD"

app() {
    case $# in
    1)
        which $1 > /dev/null && echo $1
        ;;
    2)
        which $1 > /dev/null && echo $2
        ;;
    esac
}

appon() {
    pgrep $1 >/dev/null && app $*
}

appoff() {
    pgrep $1 >/dev/null || app $*
}


menu_config() { # {{{1
    selection=`{
        app "$WM"
        app 'dmenu'
        app 'xmobar'
        echo 'wmscripts/config'
        echo 'Xdefaults'
        echo 'shrc'
        echo 'xinitrc'
        echo 'doc menu'
        app  'xscreensaver-demo'    'screensaver'
        app  'switch2'              'GTK theme'
        app  'qtconfig-qt4'         'QT4 theme'
        app  'qtconfig'             'QT3 theme'
        app  'nvidia-settings'      'nVidia'
        echo 'monitor off in 5min'
        echo 'monitor off in 15min'
        echo 'monitor off in 20min'
        app  'fvwm'
        app  'xmonad'
        app  'xfce4-settings-manager'
        [ "$WM" = 'fvwm' ] && echo 'restart fvwm'
        app 'xmonad'                'ch 2 xmonad'
        app 'fvwm'                  'ch 2 fvwm'
    } | $MENU_APP`

    case $selection in
    'fvwm' )                    exec $MY_EDITOR ~/.fvwm/config;;
    'xmonad' )                  exec $MY_EDITOR ~/.xmonad/xmonad.hs ;;
    'dmenu' )                   exec $MY_EDITOR ~/bin/menu.sh ;;
    'xinitrc' )                 exec $MY_EDITOR ~/.xinitrc ;;
    'Xdefaults' )               exec $MY_EDITOR ~/.Xdefaults; exec xrdb ~/.Xdefaults;;
    'shrc' )                    exec $MY_EDITOR ~/.shrc ;;
    'screensaver' )             exec nice -n 10 xscreensaver-demo ;;
    'monitor off in 5min' )     exec xset dpms 300 ;;
    'monitor off in 15min' )    exec xset dpms 900 ;;
    'monitor off in 20min' )    exec xset dpms 1200 ;;
    'GTK theme' )               exec switch2 ;;
    'QT4 theme' )               exec qtconfig-qt4 ;;
    'QT3 theme' )               exec qtconfig ;;
    'nVidia' )                  exec nvidia-settings ;;
    'restart fvwm' )            exec FvwmCommand restart ;;
    'ch 2 fvwm' )               exec sed -I.bak -e 's/^export WM=.*/export WM=fvwm/' ~/src/dot.files/dot.xinitrc ;;
    esac
    exit
} # 1}}}

# MAIN MENU {{{1
selection=`{
    echo 'browser'
    echo 'editor'
    echo 'terminal'
    app 'firefox'
    app 'firefox'       'firefox (private)'
    app 'firefox'       'firefox (Evitux)'
    app 'firefox'       'firefox profiles'
    app 'audacious'
    app 'chrome'
    app 'chrome'        'chrome (private)'
    app 'xombrero'
    app 'mmex'          'moneymanagerex'
    app 'urxvt'
    app 'vim'
    app 'workbench'     'sqlworkbench'
    app 'sqlworkbench'  'sqlworkbench'
    app 'e'
    app 'gimp'
    app 'liferea'
    app 'worker'
    app 'mutt'
    app 'thunar'
    app 'yed'
    app 'rox'
    app 'ardour2'
    app 'rosegarden'
    app 'gentoo'
    app 'mypaint'
    app 'sqliteman'
    echo 'play'
    app 'inkscape'
    app 'keepassx'
    app 'gvim'          'svim'
    app 'geeqie'
    app 'gpicview'
    app 'epdfview'
    app 'qpdfview'
    app 'xpdf'
    app 'mirage'
    app 'audacity'
    app 'irssi'
    app 'rawtherapee'
    app 'claws-mail'
    app 'claws-mail'    'compose'
    app 'gcalctool'
    app 'klavaro'
    app 'scilab'
    app 'filezilla'
    app 'darktable'
    echo 'calc'
    app 'gpa'
    app 'stellarium'
    app 'xfontsel'
    app 'gcolor2'
    app 'openshot'
    app 'libreoffice'
    app 'gnuplot'
    app 'mathomatic'
    app 'xterm'
    app 'urxvt'
    app 'urxvtc'
    appoff 'transmission-daemon'
    appon 'transmission-daemon'  '!transmission-daemon'
    app 'transmission-qt'   'transmission'
    app 'xchat'
    app 'tmux'
    app 'vlc'
    app 'when'
    app 'nvidia-settings'
    app 'spass'

    app 'VirtualBox'    'virtualbox'

    app 'nexuiz-glx'
    app 'warzone2100'
    app 'nexuiz'
    app 'vegastrike'
    app 'wireshark'

    app 'audacious'      'next'
    app 'audacious'      'previous'
    app 'audacious'      'pause'

    app 'setxkbmap'     'ru'
    app 'setxkbmap'     'ru (std)'
    app 'setxkbmap'     'lv'
    app 'setxkbmap'     'лж'
    app 'setxkbmap'     'дм'

    echo 'screenshot'
    echo 'area screenshot'

    echo 'config'

    app 'xscreensaver'  'screensaver'
    app 'xscreensaver'  'toggle screensaver'
    app 'AstoMenace'

    echo 'fvwm console'
    echo 'fvwm log'

    if echo "$HOSTNAME" | grep "graudeejs" > /dev/null; then
        snd_unit="$(sysctl -n hw.snd.default_unit)"
        if [ "$snd_unit" = "4" ]; then
            echo "snd -> headphones"
        elif [ "$snd_unit" = "5" ]; then
            echo "snd -> speakers"
        else
            echo "snd -> headphones"
            echo "snd -> speakers"
        fi
    fi

    echo 'SHUTDOWN'
    echo 'REBOOT'
    echo 'SLEEP'
    echo '!!! KILL !!!'

} | $MENU_APP`


case $selection in
'' ) ;;
'REBOOT' )              exec /sbin/shutdown -r now ;;
'SHUTDOWN' )            exec /sbin/shutdown -p now ;;
'firefox profiles' )    exec firefox -ProfileManager -new-instance ;;
'area screenshot' )     exec mkScreenshot.sh --area --show;;
'browser' )             exec firefox ;;
'calc' )                exec $TERMINAL -name bc -e bc -l ;;
'editor' )              exec ${GUI_EDITOR:-${TERMINAL:-xterm} -e ${EDITOR:-vi}} ;;
'terminal' )            exec ${TERMINAL:-xterm} ;;
'chrome (private)' )    exec chrome --incognito ;;
'chrome')               exec chrome $CHROME_CMDL ;;
'compose' )             exec claws-mail --compose ;;
'config' )              menu_config ;;
'mutt' )                exec $TERMINAL -name mutt -e neomutt -y ;;
'firefox' )             exec firefox;;
'fvwm console' )        exec FvwmCommand "Function Fvwm_Console" ;;
'fvwm log' )            exec $TERMINAL -name 'fvwm log' -e tail -f $HOME/.xsession-errors ;;
'gnuplot' )             exec $TERMINAL -name gnuplot -e gnuplot ;;
'irssi' )               exec $TERMINAL -name irssi -e irssi ;;
'lv' | 'лж' | 'дм' )    exec setxkbmap lv ;;
'mathomatic' )          exec $TERMINAL -name mathomatic -e mathomatic ;;
'libreoffice' )         exec libreoffice ;;
'tmux' )                exec $TERMINAL -name tmux -e tmux ;;

'next' )                exec audacious --fwd ;;
'pause' )               exec audacious --play-pause ;;
'previous' )            exec audacious --rew ;;

'ru (std)' )            exec setxkbmap ru ;;
'ru' )                  exec setxkbmap ru phonetic ;;
'scilab' )              exec $TERMINAL -name scilab -e scilab -nwni -nb ;;
'screensaver' )         exec screensaverCtl.sh lock ;;
'screenshot' )          exec mkScreenshot.sh show ;;
'term' )                exec $TERMINAL ;;
'toggle screensaver' )  exec screensaverCtl.sh toggle ;;
'gpa' )                 exec gpa -k ;;
'transmission')         if pgrep -a trayer stalonetray > /dev/null || [ $WM = 'wmfs' ]; then TR_FLAGS='-m'; else TR_FLAGS=''; fi; exec nice -n 20 transmission-qt $TR_FLAGS ;;
'transmission-daemon')  exec nice -n 20 transmission-daemon ;;
'svim' )                exec gvim --servername VIM ;;
'vim' )                 exec $TERMINAL -name vim -e vim ;;
'virtualbox' )          kldstat | grep vboxdrv > /dev/null || sudo kldload vboxdrv; exec VirtualBox ;;
'when' )                when --warp=0 --nopaging --monday_first --noampm | exec message.sh -name when -buttons "_OK" -default '_OK' -file - ;;
'spass' )               spass -s 'a-hjkmnp-zA-HJKMNP-Z2-9' -l 24 | xclip && exec message.sh -name spass -buttons '_OK' -default '_OK' 'password generated' ;;
'snd -> speakers' )     exec sudo /sbin/sysctl hw.snd.default_unit=4 ;;
'snd -> headphones' )   exec sudo /sbin/sysctl hw.snd.default_unit=5 ;;
'wtf '* )               exec message.sh "`wtf "$(echo "$selection" | sed 's/^wtf //')"`" ;;

'http://'* | 'https://'* ) open $selection ;;
*)                      exec $selection ;;
esac
# 1}}}

# vim: set ts=4 sw=4:
