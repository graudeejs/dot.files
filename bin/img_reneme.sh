#!/bin/sh

IMG_FORMATS="*.png *.jpg *.jpeg *.gif *.bmp *.tif *.tiff *.tga *.PNG *.JPG *.JPEG *.GIF *.BMP *.TIF *.TIFF *.TGA"
IMG_FORMATS="$IMG_FORMATS `echo $IMG_FORMATS | tr 'a-z' 'A-Z'`"

j=0
for i in `ls $IMG_FORMATS 2> /dev/null`; do
	chmod -x $i
	mv "$i" `printf '%0004d.%s' "$j" "$(echo $i | sed -e 's#^.*\.##' | tr 'A-Z' 'a-z')"`
	j=$(($j + 1))
done
exit 0
