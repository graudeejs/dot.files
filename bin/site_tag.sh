#!/bin/sh

rev="`site_revision.sh $1`"
if [ "$rev" != '' ]; then
  git tag --contains "$rev"
fi
