#!/bin/sh

# "THE BEER-WARE LICENSE":
# Aldis Berjoza wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.  <graudeejs@gmail.com>

# This script is used by xautolock to lock screen

[ -f "$XDG_CONFIG_HOME/wmscripts/config" ] && . "$XDG_CONFIG_HOME/wmscripts/config"
killall -s HUP gpg-agent
swLang default
exec xlock +allowroot -nice ${SCREENSAVER_NICE:-10} -lockdelay 60 -timeout 60 -startCmd startScreensaverCMDS.sh -endCmd stopScreensaverCMDS.sh

