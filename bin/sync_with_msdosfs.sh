#!/bin/sh

if [ "$1" == '' -o "$2" == '' ]; then
  echo "`basename $0` src_dir dst_dir" > /dev/stderr
  exit 1
fi

rsync --progress --stats --modify-window=1 --update --times -h -r  "$1" "$2"
