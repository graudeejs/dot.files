#!/bin/sh

# "THE BEER-WARE LICENSE":
# Aldis Berjoza wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.  <graudeejs@gmail.com>


# wraper script to ImageMagick and GraphicsMagick
# args - anything that works for ImageMagick or GraphicsMagic

if which import > /dev/null; then
	$*
	exit $?
elif which gm > /dev/null; then
	# GraphicsMagick has akwarded return codes
	gm $* && exit 1 || exit 0 
fi
echo "ImageMagick and GraphicsMagic aren't installed" > /dev/stderr
exit 1
