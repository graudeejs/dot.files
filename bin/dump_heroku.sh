#!/bin/sh

OPTIONS=""

if [ "$1" != '' ]; then
    OPTIONS="--remote $1"
fi

OUT_FILE="$1-`date '+%Y.%m.%d'`.dump.gpg"
RECIPIENT='aldis@berjoza.lv'

heroku pg:backups:capture $OPTIONS \
    && curl `heroku pg:backups:public-url $OPTIONS` | gpg2 --encrypt --recipient "$RECIPIENT" -o "$OUT_FILE"
