#!/bin/sh

PC="`hostname | tr '.' '-'`"
cd '/'
tarsnap \
    -c -f "${PC}--config-$(date +%Y-%m-%d_%H-%M-%S)" \
    --keyfile "${HOME}/.tarsnap/keys/secrets.key" \
    --cachedir "${HOME}/.tarsnap/cache" \
    'etc' \
    'usr/local/etc'
