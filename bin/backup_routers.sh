#!/bin/sh

DATE="`date +%Y%m%d`"

ROUTERS="r2m1 r2s1"
BACKUP_DIR="$HOME/Documents/router/"
mkdir -p "$BACKUP_DIR"

for router_name in $ROUTERS; do
    ssh "${router_name}" "/system backup save dont-encrypt=yes name=${router_name}-${DATE}"

    backup_file="$BACKUP_DIR/${router_name}-${DATE}.backup"

    sftp "${router_name}:${router_name}-${DATE}.backup" "$backup_file"
    gpg2 --yes --encrypt "$backup_file" && rm "$backup_file"
    sleep 1
    ssh "${router_name}" "/file remove ${router_name}-${DATE}.backup"
    echo "${router_name} backup saved"
done
