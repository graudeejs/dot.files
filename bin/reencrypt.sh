#!/bin/sh

for FILE in "$@"; do
    gpg --decrypt "$FILE" | gpg --recipient 4E00F80AFB5649E535AFE56FF6707CE4B6E4ECE3 --encrypt --output "${FILE}.new"
    [ $? -eq 0 ] && mv -f "${FILE}.new" "$FILE" || rm -f "${FILE}.new"
done
