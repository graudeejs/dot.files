#!/bin/sh

SETTINGS="$(pass show $1)"

get_val() {
    echo "$SETTINGS" | awk -F ':' '$1 == "'$1'" { print $2 }' | sed -r -e 's/^[[:space:]]+//' -e 's/[[:space:]]+$//'
}

error() {
    echo "$1" > /dev/stderr
    exit 1
}

PASSWORD="$(echo "$SETTINGS" | head -n 1)"
USERNAME="$(get_val 'username')"
HOST="$(get_val 'host')"
DB_NAME="$(get_val 'database')"
DB_TYPE="$(get_val 'adapter')"
SERVER="$(get_val 'ssh')"
PORT="$(get_val 'port')"

CMD=""

case "$DB_TYPE" in
'mysql' | 'mysql2')
    [ "$PORT" = '' ] && PORT=3306
    CMD="mysqldump --user='${USERNAME}' --password='${PASSWORD}' --port='${PORT}' --host='${HOST}' '${DB_NAME}'"
    ;;
'postgresql' | 'pg')
    [ "$PORT" = '' ] && PORT=5432
    CMD="PGPASSWORD='${PASSWORD}' pg_dump -Fc --no-acl --no-owner --username='${USERNAME}' --host='${HOST}' --port='$PORT' --dbname='${DB_NAME}'"
    ;;
*)
    error "Unsupported DB '${DB_TYPE}'"
    ;;
esac

if [ "$SERVER" != "" ]; then
    ssh "$SERVER" /bin/sh -c "\"$CMD | gzip\"" | gunzip | gpg2 --encrypt
else
    $CMD | gpg2 --encrypt
fi
