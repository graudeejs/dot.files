#!/bin/sh

calc_to_int() {
    echo "$1" | bc -l | sed -E 's/\..*$//'
}
MY_UID=${UID:-$(id -u $(whoami))}

WATERMARK_PATH="/run/user/$MY_UID/watermarks"
OUTDIR='watermarked'
mkdir -p "$WATERMARK_PATH"
mkdir -p "$OUTDIR"

for IMAGE in $*; do

    INFO=$(identify -format '%w %h %[EXIF:DateTime]' "$IMAGE")
    WIDTH=$(echo "$INFO" | cut -d ' ' -f 1)
    HEIGHT=$(echo "$INFO" | cut -d ' ' -f 2)
    YEAR=$(echo "$INFO" | cut -d ' ' -f 3 | cut -d ':' -f -1)

    if [ "$YEAR" = '' ]; then
        YEAR=$(date '+%Y')
    fi

    COPY_TEXT="© $YEAR aldis.berjoza.lv"

    if [ $WIDTH -gt $HEIGHT ]; then
        PS=$(calc_to_int "$HEIGHT * 0.03")
    else
        PS=$(calc_to_int "$HEIGHT * 0.02")
    fi

    if [ $PS -lt 30 ]; then
        PS=30
    fi

    WATERMARK="$WATERMARK_PATH/${YEAR}_$PS.gif"

    if [ ! -f "$WATERMARK" ]; then
        convert -background transparent -fill white \
            -font "$HOME/.fonts/IndieFlower.ttf" -pointsize "$PS" "label:$COPY_TEXT" \
            "$WATERMARK"
    fi

    OFFSET=$(calc_to_int "$HEIGHT * 0.02")

    # composite -dissolve 40 -gravity SouthEast -geometry '+150+100' \
    #     "$WATERMARK" "$IMAGE" "$OUTDIR/$IMAGE"
    composite -dissolve 40 -gravity SouthEast -geometry "+$OFFSET+$OFFSET" \
        "$WATERMARK" "$IMAGE" "$OUTDIR/$IMAGE"
done
