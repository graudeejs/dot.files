#!/bin/sh
export PASSWORD_STORE_DIR="$HOME/.password-store"

if [ -f .ansible-project ]; then
    PROJECT_NAME=$(cat .ansible-project | head -n 1 | sed "s/\n//")
else
    PROJECT_NAME=$(basename "$(pwd)")
fi

VAULT_ID=${VAULT_ID:-prod}
PROJECT="${PROJECT_OVERRIDE:-$PROJECT_NAME}"
if [ "$VAULT_ID" != "" -a -f "${PASSWORD_STORE_DIR}/ansible-vault/${PROJECT}/${VAULT_ID}.gpg" ]; then
    pass show "ansible-vault/$PROJECT/${VAULT_ID}" | head -n 1
elif [ -f "${PASSWORD_STORE_DIR}/ansible-vault/${PROJECT}.gpg" ]; then
    pass show "ansible-vault/$PROJECT" | head -n 1
else
    exit 1
fi
