#!/bin/sh

SETTINGS="$(pass show $1)"

get_val() {
    echo "$SETTINGS" | awk -F ':' '$1 == "'$1'" { print $2 }' | sed -r -e 's/^[[:space:]]+//' -e 's/[[:space:]]+$//'
}

error() {
    echo "$1" > /dev/stderr
    exit 1
}

PASSWORD="$(echo "$SETTINGS" | head -n 1)"
USERNAME="$(get_val 'username')"
HOST="$(get_val 'host')"
SERVER="$(get_val 'ssh')"
DB_NAME="$(get_val 'database')"
DB_TYPE="$(get_val 'adapter')"
PORT="$(get_val 'port')"

case $DB_TYPE in
'mysql' | 'mysql2')
    [ "$PORT" = '' ] && PORT=3306
    CMD="mysql --user='${USERNAME}' --password='${PASSWORD}' --host='${HOST}' --port='${PORT}' '${DB_NAME}'"
    ;;
*)
    error "Unsupported DB ${DB_TYPE}"
    exit 1
    ;;
esac

if [ "$SERVER" = "" ]; then
    $CMD
else
    ssh -tt "$SERVER" $CMD
fi
