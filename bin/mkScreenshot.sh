#!/bin/sh

# "THE BEER-WARE LICENSE":
# Aldis Berjoza wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.  <graudeejs@gmail.com>

# # Simple script to make screenshots
# args - see below case statement

DATE_FORMAT='%Y.%m.%d_at_%H.%M.%S'
QUALITY=100
FORMAT='png'
IMGDIR="$HOME/pic/screenshots"
FRAME=""
AREA='-window root'

while [ "$1" ]; do
	case $1 in
	'sleep' )
		# Sleep $2 seconds before taking screenshot
		sleep $2
		shift 2
		;;

	'window' )
		# take screenshot of window id=$2
		WID=${2:-root}
		AREA="-window $WID"
		shift 2
		;;

	'frame' )
		# take window screenshot with frame
		FRAME='-frame'
		shift
		;;

	'quality' )
		# change screenshort quality
		QUALITY=${2:-$QUALITY}
		shift 2
		;;

	'format' )
		# change image format
		FORMAT="${2:-$FORMAT}"
		shift 2
		;;

	'timestamp-format' )
		# change file name format
		DATE_FORMAT="${2:-$DATE_FORMAT}"
		shift 2
		;;

	'echo-filename' )
		# echo filename of new screenshot, when its taken
		ECHO_FNAME=1
		shift
		;;

	'img-dir' )
		# change default screenshot directory
		IMGDIR="${2:-$IMGDIR}"
		shift 2
		;;

	'area' )
		# make screenshot of area on screen
		AREA=""
		shift
		;;

	'show' )
		# show image with $IMG_VIEWER
		SHOW=1
		shift
		;;

	* )
		echo "ERR: unsuported option: $1" > /dev/stderr
		shift
		;;
	esac
done

mkdir -p "$IMGDIR"

IMGNAME="$IMGDIR/`date +$DATE_FORMAT`.$FORMAT"

magick.sh import -silent -quality $QUALITY $AREA $FRAME "$IMGNAME"

[ $ECHO_FNAME ] && echo "$IMGNAME"
[ $SHOW ] && [ "$IMG_VIEWER" ] && exec "$IMG_VIEWER" "$IMGNAME"
