#!/bin/sh

export GNUPGHOME="${GNUPGHOME:-$HOME/.gnupg}"
mkdir -p "$GNUPGHOME"
chmod 0700 "$GNUPGHOME"

cat > "$GNUPGHOME/gpg-agent.conf" <<EOF
default-cache-ttl 600
max-cache-ttl 7200
enable-ssh-support
EOF

cat > "$GNUPGHOME/gpg.conf" <<EOF
trust-model tofu+pgp
no-secmem-warning
no-greeting

require-cross-certification
keyserver hkps://keys.openpgp.org
photo-viewer "gwenview %i"

with-fingerprint

no-emit-version
keyid-format 0xlong
list-options show-uid-validity
verify-options show-uid-validity

no-allow-non-selfsigned-uid

personal-compress-preferences zlib
personal-cipher-preferences AES256
personal-digest-preferences SHA512

disable-cipher-algo 3DES
weak-digest SHA1

s2k-cipher-algo AES256
s2k-mode 3
s2k-count 65011712

default-key 4E00F80AFB5649E535AFE56FF6707CE4B6E4ECE3
#hidden-encrypt-to 4E00F80AFB5649E535AFE56FF6707CE4B6E4ECE3
#comment https://aldis.berjoza.lv/en/gpg/
EOF

curl 'https://aldis.berjoza.lv/keys/gpg.txt' | gpg2 --import
gpg2 --card-status || echo "Check that you are in scard or some similar group" > /dev/stderr

kill -s kill `pgrep gpg-agent`
echo "restart your shell!"
