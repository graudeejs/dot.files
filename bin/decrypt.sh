#!/bin/sh

if [ "$1" = '' ]; then
    echo "File not specified" > /dev/stderr
    exit 1
fi

openssl aes-256-cbc -d -a -in "$1" -out "$1.new"
