#!/bin/sh

# Some WM's (dwm and xmonad for example) need to execute some X commands
# themselves (Running from .xinitrc won't work). Put these commands here

xset r rate 250 45 b off dpms 900 1200 1500
if [ -f "$HOME/.Xdefaults" ]; then
  xrdb "$HOME/.Xdefaults"
fi
xsetroot -cursor_name left_ptr
swLang default
screensaverCtl.sh start
