#!/bin/sh

if [ $# -eq 0 ]; then
	echo "Need zpool name as argument" > /dev/stderr
	exit 1
fi

mount | awk '/^'$1'\// {print $3}' | sort -r | xargs umount
