. "$HOME/.common_env"

# set ENV to a file invoked each time sh is started for interactive use.
ENV="$HOME/.shrc"; export ENV

# vim: set ft=sh :
